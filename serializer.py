import marshal
import json
import pandas as pd
import csv
from io import StringIO # to work with in-memory string as file


test_data = []

def add_test_person(name, phone, address, age):
    test_data.append({'Name': name, 'Phone': phone, 'Address': address, 'Age': age})

add_test_person('John', '+358402454353', 'Helsinki', 42)
add_test_person('Mike', '+358401234567', 'Helsinki', 30)
add_test_person('Susan', '+12011334237', 'Malysia', 18)


# writes list of dicts to csv file
def write_to_csv(data, file):
    writer = csv.DictWriter(file, fieldnames = ['Name', 'Phone', 'Address', 'Age'])
    writer.writeheader()
    for row in data:
        writer.writerow(row)


# reads list of dicts from csv file
def read_from_csv(file):
    reader = csv.DictReader(file)
    data = []
    for row in reader:
        person = {}
        person['Name'] = row['Name']
        person['Phone'] = row['Phone']
        person['Address'] = row['Address']
        person['Age'] = int(row['Age'])
        data.append(person)
    return data


# testing wring and reading to/from file
with open('data.csv', 'w') as file:
    write_to_csv(test_data, file)
with open('data.csv', 'r') as file:
    data = read_from_csv(file)
    assert(data == test_data)


# to add support for additional storage format developer should implement new class with serialize and deserialize methods
class CsvSerializer:
    def __init__(self):
        self.format_name = "csv"

    def serialize(self, data):
        buffer = StringIO()
        write_to_csv(data, buffer)
        return buffer.getvalue()

    def deserialize(self, serialized_data):
        buffer = StringIO(serialized_data)
        return read_from_csv(buffer)


class JsonSerializer:
    def __init__(self):
        self.format_name = "json"

    def serialize(self, data):
        return json.dumps(data)

    def deserialize(self, serialized_data):
        return json.loads(serialized_data)


class BinSerializer:
    def __init__(self):
        self.format_name = "binary"

    def serialize(self, data):
        return marshal.dumps(data)

    def deserialize(self, serialized_data):
        return marshal.loads(serialized_data)


# supply an alternative reader/writer for one of the supported formats (csv)
class PandasSerializer:
    def __init__(self):
        self.format_name = "csv"

    def serialize(self, data):
        return pd.DataFrame(data, columns = ['Name', 'Phone', 'Address', 'Age'])

    def deserialize(self, serialized_data):
        return serialized_data.to_dict('records') 


# all reader/writers (2 csv, json, and binary formats):
serializers = []
serializers.append(JsonSerializer())
serializers.append(BinSerializer())
serializers.append(PandasSerializer())
serializers.append(CsvSerializer())


# query a list of currently supported formats
def get_supported_formats():
    formats = set() # without duplicates
    for serializer in serializers:
        formats.add(serializer.format_name)
    return formats


print('Supported formats:', get_supported_formats())


# testing serializers:

def test_serialization(serializer, data):
    print("\nTesting: " + serializer.format_name)
    serialized_data = serializer.serialize(data)
    print("Output:", serialized_data) # displaying the data in serialized format
    deserialized_data = serializer.deserialize(serialized_data)
    # serializing and deserializing test data back and forth in memory
    assert(data == deserialized_data) # valdiating that parsed back data is the same as input


tests_counter = 0
for serializer in serializers:
    test_serialization(serializer, test_data)
    tests_counter = tests_counter + 1
    print("Completed tests: " + str(tests_counter))
